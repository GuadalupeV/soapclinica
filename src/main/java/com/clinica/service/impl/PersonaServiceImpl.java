package com.clinica.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.clinica.dao.IPersonaDAO;
import com.clinica.model.Persona;
import com.clinica.service.IPersonaService;

@WebService
@Named
//@SOAPBindig(style = Style.RPC)
public class PersonaServiceImpl implements IPersonaService, Serializable{

	@EJB
	private IPersonaDAO dao;
		
	@WebMethod
	@Override
	public Integer registrar(Persona per) throws Exception {
		int rpta = dao.registrar(per);
		return rpta > 0 ? 1 : 0;
	}

	@WebMethod
	@Override
	public Integer modificar(Persona per) throws Exception {
		int rpta = dao.modificar(per);
		return rpta > 0 ? 1 : 0;
	}

	@WebMethod
	@Override
	public List<Persona> listar() throws Exception {
		return dao.listar();
	}

	@WebMethod
	@Override
	public Persona listarPorId(Persona t) throws Exception {
		return dao.listarPorId(t);
	}

}
