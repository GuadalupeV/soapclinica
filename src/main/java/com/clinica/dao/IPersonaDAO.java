package com.clinica.dao;

import javax.ejb.Local;

import com.clinica.model.Persona;

@Local
public interface IPersonaDAO extends ICRUD<Persona> {

}
